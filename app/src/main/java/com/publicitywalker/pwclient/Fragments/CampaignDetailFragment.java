package com.publicitywalker.pwclient.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Polygon;
import com.publicitywalker.pwclient.Adapters.AdSpinnerAdapter;
import com.publicitywalker.pwclient.Adapters.FragmentListAdapter;
import com.publicitywalker.pwclient.Holders.FragmentListHolder;
import com.publicitywalker.pwclient.Holders.ZoneListHolder;
import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Model.Zone;
import com.publicitywalker.pwclient.R;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by juancho on 1/08/17.
 */

public class CampaignDetailFragment extends Fragment{
    private static String USER_TAG="user";
    private AutoCompleteTextView nameView,descriptionView;
    private RecyclerView adsRecycler,zonesRecycler;
    private Spinner adsSpinner,zonesSpinner;
    private TextView noZonesPrompt,noAdsPrompt;
    private TextView valueFrequency,valuePriority,valueInvestment;
    private SeekBar seekbarFreqency,seekbarPriority,seekbarInvestment;
    private ProgressBar progressBar;
    private List<Ad> adList = new ArrayList<>();
    private List<Zone> zoneList;
    private AdSpinnerAdapter adSpinnerAdapter;
    private List<Ad> selectedAdList = new ArrayList<>();
    private List<Zone> selectedZoneList = new ArrayList<>();
    LinearLayout adsLinear;
    LinearLayout zonesLinear;
    private TextView acceptButton,cancelButton;
    private CampaignDetailListener listener;
    private String currentThumbnail;
    User user;


    public static CampaignDetailFragment newInstance(User user,CampaignDetailListener listener){

        CampaignDetailFragment fragment = new CampaignDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        fragment.setListener(listener);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.campaign_detail_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        user = (User) getArguments().getSerializable(USER_TAG);
        nameView = (AutoCompleteTextView) view.findViewById(R.id.campaign_name);
        descriptionView = (AutoCompleteTextView) view.findViewById(R.id.campaign_description);
       // adsRecycler = (RecyclerView) view.findViewById(R.id.ads_recycler);
       // zonesRecycler = (RecyclerView) view.findViewById(R.id.zones_recycler);
        adsLinear = (LinearLayout) view.findViewById(R.id.ads_linear);
        zonesLinear = (LinearLayout) view.findViewById(R.id.zones_linear);
        adsSpinner = (Spinner) view.findViewById(R.id.ads_spinner);
        zonesSpinner = (Spinner) view.findViewById(R.id.zones_spinner);
        noAdsPrompt = (TextView) view.findViewById(R.id.not_ads_prompt);
        noZonesPrompt = (TextView) view.findViewById(R.id.not_zones_prompt);
        valueFrequency = (TextView) view.findViewById(R.id.value_frequency);
        valueInvestment = (TextView) view.findViewById(R.id.value_investment);
        valuePriority = (TextView) view.findViewById(R.id.value_priority);
        seekbarFreqency = (SeekBar) view.findViewById(R.id.seekbar_frequency);
        seekbarInvestment = (SeekBar) view.findViewById(R.id.seekbar_investment);
        seekbarPriority = (SeekBar) view.findViewById(R.id.seekbar_priority);
        acceptButton = (TextView) view.findViewById(R.id.campaign_accept_button);
        cancelButton = (TextView) view.findViewById(R.id.campaign_cancel_button);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
/*
        adsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        zonesRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
*/
        final FragmentListAdapter adRecyclerAdapter = new FragmentListAdapter(getObjectList(selectedAdList));
        final FragmentListAdapter zoneRecyclerAdapter = new FragmentListAdapter(getObjectList(selectedZoneList));
//        adsRecycler.setAdapter(adRecyclerAdapter);
//        zonesRecycler.setAdapter(zoneRecyclerAdapter);


        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nameView.getText().toString().length()==0){
                    nameView.setError(getString(R.string.error_no_name));
                    Toast.makeText(getContext(),getString(R.string.error_no_name),Toast.LENGTH_SHORT).show();
                }else if (descriptionView.getText().toString().length()==0){
                    descriptionView.setError(getString(R.string.error_no_description));
                    Toast.makeText(getContext(),getString(R.string.error_no_description),Toast.LENGTH_SHORT).show();
                }else {
                    Campaign campaign = new Campaign(nameView.getText().toString(), descriptionView.getText().toString(), Double.parseDouble(valueInvestment.getText().toString()), Integer.parseInt(valuePriority.getText().toString()), Integer.parseInt(valueFrequency.getText().toString()), selectedZoneList, selectedAdList, user.getCompanyId(), currentThumbnail);
                    progressBar.setVisibility(View.VISIBLE);
                    if (listener != null)
                        listener.OnCreateNewCampaign(campaign);
                }

            }
        });

        ArrayAdapter<Ad> adAdapter = new ArrayAdapter<Ad>(getContext(),android.R.layout.simple_spinner_item,adList);
        adsSpinner.setAdapter(adAdapter);
        adsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if( position>0) {
                    final Ad ad = adList.get(position);
                    if(!selectedAdList.contains(ad)) {
                        final View v = LayoutInflater.from(getContext()).inflate(R.layout.campaign_ad_row,null);
                        FragmentListHolder holder = new FragmentListHolder(v);
                       holder.title.setText(ad.getName());
                        holder.description.setText(ad.getDescription());
                        if(ad.getThumbnail().length()>0)
                            Picasso.with(getContext()).load(ad.getThumbnail()).fit().into(holder.image);
                        else
                            Picasso.with(getContext()).load(R.drawable.video).fit().into(holder.image);

                       currentThumbnail = ad.getThumbnail();
                        adsLinear.addView(v);
                        noAdsPrompt.setVisibility(View.GONE);
                        selectedAdList.add(ad);

                        holder.delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                adsLinear.removeView(v);
                                selectedAdList.remove(ad);

                                if(adsLinear.getChildCount()==0)
                                    noAdsPrompt.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<Zone> zoneAdapter = new ArrayAdapter<Zone>(getContext(),android.R.layout.simple_spinner_item,zoneList);
        if(zoneList==null)
            zoneList = new ArrayList<>();
        zonesSpinner.setAdapter(zoneAdapter);
        zonesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position>0){
                    final Zone zone = zoneList.get(position);
                    if(!selectedZoneList.contains(zone)){
                        selectedZoneList.add(zone);
                        noZonesPrompt.setVisibility(View.GONE);
                        final View v = LayoutInflater.from(getContext()).inflate(R.layout.campaign_zone_row,null);
                        ZoneListHolder holder = new ZoneListHolder(v);

                        holder.zone = zone;
                        holder.title.setText(zone.getName());
                        holder.description.setText(zone.getDescription());
                        holder.mapView.onCreate(null);
//                        holder.parent.setTag(holder);
                        //holder.parent.setOnClickListener(this);


                        if(holder.mapView!=null)
                            holder.mapView.getMapAsync(new OnMapReadyCallback() {
                                @Override
                                public void onMapReady(GoogleMap googleMap) {

                                    Polygon polygon = googleMap.addPolygon(zone.getPolygon(getContext()));
                                    int padding = 0; // offset from edges of the map in pixels
                                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(zone.getMapBounds(), padding);
                                    googleMap.moveCamera(cu);
                                }
                            });

                        zonesLinear.addView(v);

                        holder.delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                zonesLinear.removeView(v);
                                selectedZoneList.remove(zone);

                                if(zonesLinear.getChildCount()==0)
                                    noZonesPrompt.setVisibility(View.VISIBLE);
                            }
                        });

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        seekbarFreqency.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                valueFrequency.setText((progress+1)+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekbarPriority.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                valuePriority.setText(progress+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbarInvestment.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                valueInvestment.setText(progress+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    public void setListener(CampaignDetailListener listener){
        this.listener = listener;
    }

    public void setAdList(List<Ad> list){this.adList = list;

    }

    public void setZoneList(List<Zone> list){
        this.zoneList = list;
    }

    public List<Object> getObjectList(List<?> clas){
        List<Object> objlst = new ArrayList();
        for(int i= 0;i< clas.size();i++){
            objlst.add((Object) clas.get(i));
        }
        return objlst;
    }

    public void setCampaignList(List<Campaign> campaignList){
        int temp=0;
        double cam = 0;
        for(Campaign campaign1 : campaignList) {
            temp += (campaign1.getInvestment());
            cam = campaign1.getAvailablePoints();
        }

        int currentpoints = (int) (cam-temp);
        seekbarInvestment.setMax(currentpoints);
    }

    public interface CampaignDetailListener {
        void OnCreateNewCampaign(Campaign campaign);
    }
}
