package com.publicitywalker.pwclient.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.publicitywalker.pwclient.Adapters.CampaignListAdapter;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.MarkerCluster;
import com.publicitywalker.pwclient.Model.Play;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.R;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 12/08/17.
 */

public class HomeFragment extends Fragment {

    private static String USER_TAG="user";
    private List<Campaign> campaignList=new ArrayList<>();
    private TextView availablePoints,investedPoints,campaignsError;
    private RecyclerView campaignRecycler;
    private MapView map;
    private CampaignListAdapter adapter;
    private ClusterManager<MarkerCluster> clusterManager;
    private CampaignListAdapter.CampaigListAdapterlistener campaigListAdapterlistener;
    public GoogleMap googleM;

    public static HomeFragment newInstance(User user, CampaignListAdapter.CampaigListAdapterlistener campaigListAdapterlistener){
        HomeFragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        fragment.campaigListAdapterlistener = campaigListAdapterlistener;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

         availablePoints = (TextView) view.findViewById(R.id.home_available_points);
         investedPoints = (TextView) view.findViewById(R.id.home_invested_points);
         campaignsError = (TextView) view.findViewById(R.id.home_campaigns_error);
         campaignRecycler = (RecyclerView) view.findViewById(R.id.home_campaigns_recycler);
         map = (MapView) view.findViewById(R.id.home_map);
        adapter = new CampaignListAdapter(campaignList);
        adapter.setListener(campaigListAdapterlistener);
        campaignRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        campaignRecycler.setAdapter(adapter);
        campaignRecycler.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getContext())
                        .color(getContext().getColor(R.color.colorAccent))
                        .sizeResId(R.dimen.divider_size)
                        .marginResId(R.dimen.activity_horizontal_margin, R.dimen.activity_horizontal_margin)
                        .build());
        map.onCreate(savedInstanceState);
        map.getMapAsync(onMapReadyCallback);

    }

    public void populateViews(List<Campaign> campaignList){
        if(campaignList.size()>0){
            Campaign campaign = campaignList.get(0);
            double temp=0;
            for(Campaign campaign1 : campaignList)
                temp+=(campaign1.getInvestment()-campaign1.getPointsConsumed());
            investedPoints.setText(String.format("%.2f",temp));
            availablePoints.setText(String.format("%.2f",campaign.getAvailablePoints()-campaign.getPointsConsumed()-temp));


        }

    }

    public void setCampaignList(List<Campaign> campaignList){
        List<Campaign> tempList = new ArrayList<>();
        for(Campaign campaign : campaignList){
            if(campaign.isActive())
                tempList.add(campaign);
        }
        this.campaignList = campaignList;
        populateViews(tempList);
        new Thread(new Runnable() {
            @Override
            public void run() {
                updateMap();
            }
        }).start();

        if(adapter!=null)
            adapter.updateList(tempList);

        if(tempList.size()>0)
            campaignsError.setVisibility(View.GONE);
    }

    public void updateMap(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(googleM!=null){
            //clusterManager = new ClusterManager<MarkerCluster>(map.getContext(),googleM);

            final LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for(Campaign campaign:campaignList){
                List<Play>playList= campaign.getPlayList();
                for(final Play play:playList){
                    try {
                        if(play.getLatLng().latitude!=0) {
                            map.post(new Runnable() {
                                @Override
                                public void run() {
                                    googleM.addMarker(new MarkerOptions()
                                            .position(new LatLng(play.getLatLng().latitude, play.getLatLng().longitude)));
                                }
                            });

                            builder.include(play.getLatLng());

                            MarkerCluster markerCluster = new MarkerCluster(play.getLatLng().latitude, play.getLatLng().longitude);
                            clusterManager.addItem(markerCluster);
                            builder.include(play.getLatLng());
                        }

                    }catch (NullPointerException e){

                    }
                }

            }

            map.post(new Runnable() {
                @Override
                public void run() {
                    CameraUpdate cu = null;
                    try {
                        cu = CameraUpdateFactory.newLatLngBounds(builder.build(), 150);
                        googleM.moveCamera(cu);
                    }catch (Exception e){

                    }
                    googleM.setOnCameraIdleListener(clusterManager);
                    googleM.setOnMarkerClickListener(clusterManager);
                }
            });

        }
    }
    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {

            googleM = googleMap;
            googleMap.getUiSettings().setMapToolbarEnabled(false);
            googleMap.getUiSettings().setZoomControlsEnabled(true);
            map.onResume();
            map.onEnterAmbient(null);
            if(clusterManager==null)
                clusterManager = new ClusterManager<MarkerCluster>(getContext(), googleM);
        }
    };
}
