package com.publicitywalker.pwclient.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.publicitywalker.pwclient.Adapters.CampaignListAdapter;
import com.publicitywalker.pwclient.Adapters.FragmentListAdapter;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.R;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 26/07/17.
 */

public class CampaignsFragment extends Fragment {

    private static String USER_TAG="user";
    public List<Campaign> campaignList=new ArrayList<>();
    public List<Object> objectList;
    private CampaignListAdapter adapter;
    private CampaignListAdapter.CampaigListAdapterlistener campaigListAdapterlistener;

    public static CampaignsFragment newInstance(User user, CampaignListAdapter.CampaigListAdapterlistener listAdapterlistener){
        CampaignsFragment fragment = new CampaignsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        fragment.campaigListAdapterlistener = listAdapterlistener;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_recycler);
        adapter = new CampaignListAdapter(campaignList);
        adapter.setListener(campaigListAdapterlistener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getContext())
                        .color(getContext().getColor(R.color.colorAccent))
                        .sizeResId(R.dimen.divider_size)
                        .marginResId(R.dimen.activity_horizontal_margin, R.dimen.activity_horizontal_margin)
                        .build());
    }

    private List<Object> getObjectList(List<Campaign> campaigns){
        List<Object> objlst = new ArrayList();
        for(Campaign campaign : campaigns)
            objlst.add((Object)campaign);

        return objlst;
    }

    public void setCampaignList(List<Campaign> campaignList){
        this.campaignList = campaignList;
        this.objectList = getObjectList(campaignList);
        if(adapter!=null)
        adapter.notifyDataSetChanged();
    }

}
