package com.publicitywalker.pwclient.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.publicitywalker.pwclient.Adapters.FragmentListAdapter;
import com.publicitywalker.pwclient.Adapters.ZoneListAdapter;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Model.Zone;
import com.publicitywalker.pwclient.R;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 28/07/17.
 */

public class ZonesFragment extends Fragment {

    private static String USER_TAG="user";
    public List<Zone> zoneList=new ArrayList<>();
    private ZoneListAdapter adapter;
    private ZoneListAdapter.ZoneListClickLstener zoneListClickLstener;

    public static ZonesFragment newInstance(User user, ZoneListAdapter.ZoneListClickLstener zoneListClickLstener){
        ZonesFragment fragment = new ZonesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        fragment.setZonesListClickListener(zoneListClickLstener);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_recycler);

         adapter = new ZoneListAdapter(zoneList);
        adapter.setZoneListClickListener(zoneListClickLstener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getContext())
                        .color(getContext().getColor(R.color.colorAccent))
                        .sizeResId(R.dimen.divider_size)
                        .marginResId(R.dimen.activity_horizontal_margin, R.dimen.activity_horizontal_margin)
                        .build());
    }

    public void setZonesListClickListener(ZoneListAdapter.ZoneListClickLstener listener){
        this.zoneListClickLstener = listener;
    }

    public void setZoneList(List<Zone> zoneList){
        this.zoneList = zoneList;
        if(adapter!=null)
            adapter.notifyDataSetChanged();
    }






}
