package com.publicitywalker.pwclient.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.publicitywalker.pwclient.Adapters.FragmentListAdapter;
import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.R;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 27/07/17.
 */

public class AdsFragment extends GeneralFragment {

    public List<Ad> adsList=new ArrayList<>();
    public List<Object> objectList;
    private  FragmentListAdapter adapter;
    private static String USER_TAG="user";
    public static FragmentListAdapter.FragmentListAdapterListener listAdapterListener;

    public static AdsFragment newInstance(User user, FragmentListAdapter.FragmentListAdapterListener fragmentListAdapterListener){
        AdsFragment fragment = new AdsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        fragment.listAdapterListener = fragmentListAdapterListener;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_recycler);

        objectList = getObjectList(adsList);
        adapter = new FragmentListAdapter(objectList);
        adapter.setListener(listAdapterListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getContext())
                        .color(getContext().getColor(R.color.colorAccent))
                        .sizeResId(R.dimen.divider_size)
                        .marginResId(R.dimen.activity_horizontal_margin, R.dimen.activity_horizontal_margin)
                        .build());
    }

    public void setAdsList(List<Ad> adsList){
        this.adsList = adsList;
        this.objectList = getObjectList(adsList);
        if(adapter!=null)
            adapter.notifyDataSetChanged();
    }
}
