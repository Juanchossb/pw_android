package com.publicitywalker.pwclient.Fragments;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.publicitywalker.pwclient.Adapters.ZoneListAdapter;
import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Networking.NetworkManager;
import com.publicitywalker.pwclient.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * Created by juancho on 29/07/17.
 */

public class AdDetailFragment extends Fragment {
    private static String USER_TAG="user";
    private static int AD_DETAIL_TAG=2;

    private AutoCompleteTextView editName;
    private AutoCompleteTextView editDescription;
    private ImageView uploadButton;
    private TextView acceptButton;
    private TextView cancelButton;
    private File currentVideoFile;
    private File currentSnapshotFile;
    private AdsDetailListener listener;
    private ProgressBar progressBar;
    private int duration;

    public static AdDetailFragment newInstance(User user, AdsDetailListener listener){
        AdDetailFragment fragment = new AdDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        fragment.setAdsDetailsListener(listener);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.video_detail_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final User user = (User) getArguments().getSerializable(USER_TAG);

        editName = (AutoCompleteTextView) view.findViewById(R.id.video_name);
        editDescription = (AutoCompleteTextView) view.findViewById(R.id.video_description);
        uploadButton = (ImageView) view.findViewById(R.id.video_snapshot);
        acceptButton = (TextView) view.findViewById(R.id.video_accept_button);
        cancelButton = (TextView) view.findViewById(R.id.video_cancel_button);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,getString(R.string.select_video)),AD_DETAIL_TAG);

            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editName.getText().toString();
                String description = editDescription.getText().toString();

                if(name.length()==0){
                    editName.setError(getString(R.string.error_no_name));
                    Toast.makeText(getContext(),getString(R.string.error_no_name),Toast.LENGTH_SHORT).show();
                }else if(description.length()==0){
                    editDescription.setError(getString(R.string.error_no_description));
                    Toast.makeText(getContext(),getString(R.string.error_no_description),Toast.LENGTH_SHORT).show();
                }else if(currentVideoFile==null){
                    editDescription.setError(getString(R.string.error_no_file));
                    Toast.makeText(getContext(),getString(R.string.error_no_file),Toast.LENGTH_SHORT).show();
                }else {
                    Ad ad = new Ad(name, description, user.getCompanyId(), currentVideoFile, currentSnapshotFile, duration);
                    progressBar.setVisibility(View.VISIBLE);
                    if (listener != null)
                        listener.OnCreateNewAd(ad);

                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode== AD_DETAIL_TAG){


                String filePath = null;
                String fileName;
                Uri selectedImageUri = data.getData();
                String filemanagerstring = selectedImageUri.getPath();

                String selectedImagePath = NetworkManager.getPath(getContext(),selectedImageUri);

                if (!selectedImagePath.equals("")) {
                    filePath = selectedImagePath;
                } else if (!filemanagerstring.equals("")) {
                    filePath = filemanagerstring;
                }
                int lastIndex = filePath.lastIndexOf("/");
                fileName = filePath.substring(lastIndex+1);

                Bitmap bitmap = getVideoFrame(selectedImagePath);
                uploadButton.setImageBitmap(bitmap);
                try {
                    currentSnapshotFile = bitmapToFile(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                MediaPlayer mp = MediaPlayer.create(getContext(), Uri.parse(selectedImagePath));
                duration = mp.getDuration()/1000;
                mp.release();

            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        CursorLoader loader = new CursorLoader(getContext(), uri, projection, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private int getAdDuration(Uri uri){
        MediaPlayer mp = MediaPlayer.create(getContext(), uri);
        int duration = mp.getDuration();
        mp.release();
        return duration;
    }

    private File bitmapToFile(Bitmap bitmap) throws IOException {
        File file = new File(getContext().getFilesDir(), System.currentTimeMillis()+".jpg");
        file.createNewFile();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return file;
    }

    public Bitmap getVideoFrame(String path){
        //File sdcard = Environment.getExternalStorageDirectory();
        currentVideoFile = new File(path);

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        Bitmap result = null;
        try {
            retriever.setDataSource(getContext(),Uri.parse(currentVideoFile.getPath()));


            result=retriever.getFrameAtTime(10000,MediaMetadataRetriever.OPTION_CLOSEST);


        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
            }
        }
        return result;
    }

    public void setAdsDetailsListener(AdsDetailListener listener){
        this.listener = listener;
    }

    public interface AdsDetailListener{
        void OnCreateNewAd(Ad ad);
    }

}
