package com.publicitywalker.pwclient.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.publicitywalker.pwclient.Activities.ZoneCreatorActivity;
import com.publicitywalker.pwclient.Adapters.ZoneListAdapter;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Model.Zone;
import com.publicitywalker.pwclient.Networking.NetworkManager;
import com.publicitywalker.pwclient.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by juancho on 28/07/17.
 */

public class ZoneDetailFragment extends Fragment{

    private static String USER_TAG="user";
    private static int MAP_RESULT=1;
    private AutoCompleteTextView editName;
    private AutoCompleteTextView editDescription;
    private MapView mapView;
    GoogleMap map;
    private TextView acceptButton;
    private TextView cancelButton;
    private ZoneDetailListener listener;
    private User user;
    Polygon polygon;
    ProgressBar progressBar;
    Zone zone;

    public static ZoneDetailFragment newInstance(User user){
        ZoneDetailFragment fragment = new ZoneDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.zone_detail_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editName = (AutoCompleteTextView) view.findViewById(R.id.zone_name);
        editDescription = (AutoCompleteTextView) view.findViewById(R.id.zone_description);
        mapView = (MapView) view.findViewById(R.id.zone_map);
        acceptButton = (TextView) view.findViewById(R.id.zone_accept_button);
        cancelButton = (TextView) view.findViewById(R.id.zone_cancel_button);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        user = (User) getArguments().getSerializable(USER_TAG);

        mapView.onCreate(savedInstanceState);

        if(mapView!=null)
            mapView.getMapAsync(new OnMapReadyCallback() {
                @SuppressWarnings("MissingPermission")
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    map = googleMap;
                    mapView.onResume();
                    mapView.onEnterAmbient(null);
                    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            startActivityForResult(new Intent(getContext(), ZoneCreatorActivity.class),MAP_RESULT);
                        }
                    });

                    LocationManager locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
                    Criteria criteria = new Criteria();
                    String provider = locationManager.getBestProvider(criteria, true);
                    Location location = locationManager.getLastKnownLocation(provider);

                    if(location!=null) {
                        double latitude = location.getLatitude();
                        double longitude = location.getLongitude();
                        LatLng latLng = new LatLng(latitude, longitude);
                        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 14);
                        map.animateCamera(yourLocation);
                    }
                }
            });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<LatLng> list = new ArrayList<LatLng>();
                if(polygon!=null)
                list=polygon.getPoints();
                if(editName.getText().toString().length()==0){
                    editName.setError(getString(R.string.error_no_name));
                    Toast.makeText(getContext(),getString(R.string.error_no_name),Toast.LENGTH_SHORT).show();
                }else if (editDescription.getText().toString().length()==0){
                    editDescription.setError(getString(R.string.error_no_description));
                    Toast.makeText(getContext(),getString(R.string.error_no_description),Toast.LENGTH_SHORT).show();
                }else if(list.size()==0){
                    editDescription.setError(getString(R.string.error_no_zone));
                    Toast.makeText(getContext(),getString(R.string.error_no_zone),Toast.LENGTH_SHORT).show();
                }else {

                    String raw = "";
                    for (LatLng latLng : list) {
                        raw = raw + "," + latLng.latitude + " " + latLng.longitude;
                    }
                    String code = String.format("'POLYGON((%s))'", raw.substring(1));

                    progressBar.setVisibility(View.VISIBLE);
                    if (listener != null)
                        listener.OnCreateNewZone(new Zone(editName.getText().toString(), editDescription.getText().toString(), user.getCompanyId(), code));
                }
            }
        });




    }
    public void setZoneDetailListener(ZoneDetailListener listener){
        this.listener = listener;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 & data!=null){
            String json = data.getStringExtra("result");
            try {
                JSONArray jsonArray = new JSONArray(json);
                final PolygonOptions polygonOptions = new PolygonOptions();
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                polygonOptions.fillColor(getContext().getColor(R.color.colorAlphaPrimary));
                polygonOptions.strokeColor(getContext().getColor(R.color.colorAccent));
                polygonOptions.strokeWidth(2);

                for(int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    double lat = jsonObject.getDouble("latitude");
                    double lng = jsonObject.getDouble("longitude");
                    polygonOptions.add(new LatLng(lat,lng));
                    builder.include(new LatLng(lat,lng));
                }

                polygon = map.addPolygon(polygonOptions);
                int padding = 0; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(builder.build(), padding);
                map.moveCamera(cu);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public interface ZoneDetailListener {
        void OnCreateNewZone(Zone zone);
    }
}
