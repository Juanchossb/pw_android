package com.publicitywalker.pwclient.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.publicitywalker.pwclient.Adapters.FragmentListAdapter;
import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.Code;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.R;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 11/09/17.
 */

public class CodesFragment extends GeneralFragment {

    private static String USER_TAG="user";
    public List<Code> codeList=new ArrayList<>();
    public List<Object> objectList;
    private FragmentListAdapter adapter;

    public static CodesFragment newInstance(User user){
        CodesFragment fragment = new CodesFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
       // fragment.listAdapterListener = fragmentListAdapterListener;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_recycler);

        objectList = getObjectList(codeList);
        adapter = new FragmentListAdapter(objectList);
       // adapter.setListener(listAdapterListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(getContext())
                        .color(getContext().getColor(R.color.colorAccent))
                        .sizeResId(R.dimen.divider_size)
                        .marginResId(R.dimen.activity_horizontal_margin, R.dimen.activity_horizontal_margin)
                        .build());
    }

    public void setCodeList(List<Code> codeList){
        this.codeList = codeList;
        this.objectList = getObjectList(codeList);
        if(adapter!=null)
            adapter.notifyDataSetChanged();
    }

}
