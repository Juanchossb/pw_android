package com.publicitywalker.pwclient.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.publicitywalker.pwclient.Model.Code;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.R;

import java.util.Random;

/**
 * Created by juancho on 11/09/17.
 */

public class CodesDetailFragment extends Fragment {
    private static String USER_TAG="user";
    private static int CODE_LENGHT=5;

    AutoCompleteTextView ammount,userName,companyName,companyAddress,phone,notes;
    TextView botonAceptar,botonCancelar,code;
    ImageView reload;
    CodesDetailFragmentListener listener;

    public static CodesDetailFragment newInstance(User user, CodesDetailFragmentListener listener){
        CodesDetailFragment fragment = new CodesDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        fragment.setListener(listener);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.code_detail_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ammount = (AutoCompleteTextView) view.findViewById(R.id.code_amount);
        userName = (AutoCompleteTextView) view.findViewById(R.id.code_user_name);
        companyName = (AutoCompleteTextView) view.findViewById(R.id.code_company_name);
        companyAddress = (AutoCompleteTextView) view.findViewById(R.id.code_company_address);
        phone = (AutoCompleteTextView) view.findViewById(R.id.code_phone);
        notes = (AutoCompleteTextView) view.findViewById(R.id.code_notes);
        botonAceptar = (TextView) view.findViewById(R.id.code_accept_button);
        botonCancelar = (TextView) view.findViewById(R.id.code_cancel_button);
        code = (TextView) view.findViewById(R.id.code_code);
        reload = (ImageView) view.findViewById(R.id.code_reload);

        updateCode();

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCode();
            }
        });

        botonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempAmmount = ammount.getText().toString();
                String tempUsername = userName.getText().toString();
                String tempCompanyName = companyName.getText().toString();
                String tempCompanyAddress = companyAddress.getText().toString();
                String tempPhone = phone.getText().toString();
                String tempNotes = notes.getText().toString();
                String tempCode = code.getText().toString();
                Code code = null;
                try {
                    code = new Code(tempCode, Integer.parseInt(tempAmmount), tempUsername, tempCompanyName, tempCompanyAddress, tempPhone, tempNotes);
                }catch (NumberFormatException e){
                    
                }
                if(tempAmmount.length()==0){
                    ammount.setError(getString(R.string.error_empty_field));
                }
                if(tempUsername.length()==0){
                    userName.setError(getString(R.string.error_empty_field));
                }
                if(tempCompanyName.length()==0){
                    companyName.setError(getString(R.string.error_empty_field));
                }
                if(tempCompanyAddress.length()==0){
                    companyAddress.setError(getString(R.string.error_empty_field));
                }
                if(tempPhone.length()==0){
                    phone.setError(getString(R.string.error_empty_field));
                }
                if(tempNotes.length()==0){
                    notes.setError(getString(R.string.error_empty_field));
                }

                if(tempAmmount.length()>0 && tempUsername.length()>0 && tempCompanyName.length()>0 && tempCompanyAddress.length()>0 && tempPhone.length() > 0 && tempNotes.length()>0)
                if(listener!=null && code !=null)
                    listener.createCode(code);
            }
        });


    }

    private void updateCode(){
        code.setText(getRandomCode());
    }
    private String getRandomCode(){
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < CODE_LENGHT) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    private void setListener(CodesDetailFragmentListener listener){
        this.listener = listener;
    }

    public interface CodesDetailFragmentListener{
        void createCode(Code code);
    }
}
