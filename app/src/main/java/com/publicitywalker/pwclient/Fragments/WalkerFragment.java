package com.publicitywalker.pwclient.Fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.publicitywalker.pwclient.Managers.AccessPointManager;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Networking.SockerServerService;
import com.publicitywalker.pwclient.Networking.SocketClient;
import com.publicitywalker.pwclient.Networking.SocketServer;
import com.publicitywalker.pwclient.R;

import java.lang.reflect.InvocationTargetException;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_SETTINGS;

/**
 * Created by juancho on 28/08/17.
 */

public class WalkerFragment extends Fragment {

    Switch apSwitch;
    private static int REQUEST_WRITE_SETTINGS=3;
    SocketServer socketServer;
    SocketClient socketClient;
    LinearLayout walkerLine;
    Intent serviceIntent;
    public static boolean isServiceRunning = false;
    public static WalkerFragment newInstance(Context context){
        WalkerFragment fragment = new WalkerFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        //fragment.socketServer = new SocketServer(context,fragment.socketServerListener);
        fragment.socketClient = new SocketClient(fragment.socketClientListener);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      return  inflater.inflate(R.layout.walker_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apSwitch = (Switch) view.findViewById(R.id.ap_switch);
        walkerLine = (LinearLayout) view.findViewById(R.id.walker_line);
        apSwitch.setOnCheckedChangeListener(ApSwitchListener);
        youDesirePermissionCode(getActivity());
        serviceIntent = new Intent(view.getContext(),SockerServerService.class);
        socketClient.launchClient();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_WRITE_SETTINGS && Settings.System.canWrite(getContext())){
            Log.d("TAG", "CODE_WRITE_SETTINGS_PERMISSION success");
            //do your code
        }
    }

    private CompoundButton.OnCheckedChangeListener ApSwitchListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                getActivity().startService(serviceIntent); //Starting the service
                getActivity().bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
            }else{
                getActivity().unbindService(serviceConnection);
                getActivity().stopService(serviceIntent);
            }
                    //accessPointManager.disableAccessPoint();
        }
    };

    public static void youDesirePermissionCode(Activity context){
        boolean permission;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = Settings.System.canWrite(context);
        } else {
            permission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_SETTINGS) == PackageManager.PERMISSION_GRANTED;
        }
        if (permission) {
            //do your code
        }  else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + context.getPackageName()));
                context.startActivityForResult(intent, REQUEST_WRITE_SETTINGS);
            } else {
                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.WRITE_SETTINGS}, REQUEST_WRITE_SETTINGS);
            }
        }
    }

    private boolean mayRequestWriteSettings() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (getActivity().checkSelfPermission(WRITE_SETTINGS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_SETTINGS)) {
            Snackbar.make(apSwitch, R.string.permission_rationale_files, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{WRITE_SETTINGS}, REQUEST_WRITE_SETTINGS);
                        }
                        });
        } else {
            requestPermissions(new String[]{WRITE_SETTINGS}, REQUEST_WRITE_SETTINGS);
        }
        return false;
    }

    private LoaderManager.LoaderCallbacks<Cursor> loaderCallbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return null;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_WRITE_SETTINGS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //do your code
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            SockerServerService.LocalBinder binder = (SockerServerService.LocalBinder) service;
            SockerServerService myService = binder.getServiceInstance(); //Get instance of your service!
            myService.setHomeSocketServerListener(socketServerListener);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    public SocketServer.SocketServerListener socketServerListener = new SocketServer.SocketServerListener() {
        @Override
        public void OnMessageReceived(String message) {
            try {
                final TextView textView = new TextView(getContext());
                textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                textView.setTextColor(Color.BLACK);
                textView.setText(message);
                walkerLine.post(new Runnable() {
                    @Override
                    public void run() {
                        walkerLine.addView(textView);
                    }
                });
            }catch (Exception e){

            }

        }
    };

    public SocketClient.SocketClientListener socketClientListener = new SocketClient.SocketClientListener() {
        @Override
        public void sockerServerConnection(final boolean isConnected) {
            apSwitch.post(new Runnable() {
                @Override
                public void run() {
                    apSwitch.setOnCheckedChangeListener(null);
                    apSwitch.setChecked(isConnected);
                    apSwitch.setOnCheckedChangeListener(ApSwitchListener);
                    if(isConnected) {
                        getActivity().bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
                    }
                }
            });

        }
    };
}
