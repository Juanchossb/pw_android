package com.publicitywalker.pwclient.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.publicitywalker.pwclient.Activities.LoginActivity;
import com.publicitywalker.pwclient.Model.ApiResponse;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Networking.NetworkManager;
import com.publicitywalker.pwclient.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by juancho on 11/09/17.
 */

public class RedeemCodeFragment extends Fragment {

    private static String USER_TAG="user";

    AutoCompleteTextView redeemCode;
    TextView acceptButton;
    NetworkManager networkManager;
    ProgressBar progressBar;

    public static RedeemCodeFragment newInstance(User user, NetworkManager networkManager){
        RedeemCodeFragment fragment = new RedeemCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_TAG,user);
        fragment.setArguments(bundle);
        fragment.networkManager = networkManager;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.redeem_code_fragment,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final User user = (User) getArguments().getSerializable(USER_TAG);

        redeemCode = (AutoCompleteTextView) view.findViewById(R.id.redeem_code);
        acceptButton = (TextView) view.findViewById(R.id.redeem_accept_button);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                String code = redeemCode.getText().toString().toUpperCase();
                if(code.length()==0){
                    progressBar.setVisibility(View.GONE);
                    redeemCode.setError(getString(R.string.error_empty_field));
                }else {
                    if(networkManager== null)
                        networkManager = new NetworkManager();
                    networkManager.redeemCode(code,user).enqueue(codeRedemptionCallback);
                }
            }
        });

        redeemCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string=s.toString();
                if(!string.equals(string.toUpperCase()))
                {
                    string=string.toUpperCase();
                    redeemCode.setText(string);
                }
                redeemCode.setSelection(redeemCode.getText().length());
            }
        });


    }

    private Callback<ApiResponse> codeRedemptionCallback = new Callback<ApiResponse>() {
        @Override
        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
            progressBar.setVisibility(View.GONE);
            if(response.body()!=null) {
                ApiResponse.Type success = response.body().getType();
                if (success == ApiResponse.Type.SUCCESS) {
                    Toast.makeText(getContext(), getString(R.string.success_redeem), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }

            }else{
                redeemCode.setError(getString(R.string.error_invalid_code));
            }
        }

        @Override
        public void onFailure(Call<ApiResponse> call, Throwable t) {

        }
    };
}
