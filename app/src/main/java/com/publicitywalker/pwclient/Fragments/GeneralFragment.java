package com.publicitywalker.pwclient.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.publicitywalker.pwclient.Adapters.FragmentListAdapter;
import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 28/07/17.
 */

public class GeneralFragment extends Fragment {
    private static String USER_TAG="user";
    public List<Campaign> campaignList=new ArrayList<>();
    public List<Object> objectList;
    private FragmentListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.fragment_recycler);

        objectList = getObjectList(campaignList);
        adapter = new FragmentListAdapter(objectList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    public List<Object> getObjectList(List<?> clas){
        List<Object> objlst = new ArrayList();
        for(int i= 0;i< clas.size();i++){
           objlst.add((Object) clas.get(i));
        }
        return objlst;
    }

}
