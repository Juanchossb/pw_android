package com.publicitywalker.pwclient.Activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.publicitywalker.pwclient.Adapters.CampaignListAdapter;
import com.publicitywalker.pwclient.Adapters.FragmentListAdapter;
import com.publicitywalker.pwclient.Adapters.ZoneListAdapter;
import com.publicitywalker.pwclient.Fragments.AdDetailFragment;
import com.publicitywalker.pwclient.Fragments.AdsFragment;
import com.publicitywalker.pwclient.Fragments.CampaignDetailFragment;
import com.publicitywalker.pwclient.Fragments.CampaignsFragment;
import com.publicitywalker.pwclient.Fragments.CodesDetailFragment;
import com.publicitywalker.pwclient.Fragments.CodesFragment;
import com.publicitywalker.pwclient.Fragments.HomeFragment;
import com.publicitywalker.pwclient.Fragments.RedeemCodeFragment;
import com.publicitywalker.pwclient.Fragments.WalkerFragment;
import com.publicitywalker.pwclient.Fragments.ZoneDetailFragment;
import com.publicitywalker.pwclient.Fragments.ZonesFragment;
import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.ApiResponse;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.Code;
import com.publicitywalker.pwclient.Model.Error;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Model.Zone;
import com.publicitywalker.pwclient.Networking.NetworkManager;
import com.publicitywalker.pwclient.R;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    private CampaignsFragment campaignsFragment;
    private AdsFragment adsFragment;
    private ZonesFragment zonesFragment;
    private ZoneDetailFragment zoneDetailFragment;
    private AdDetailFragment adDetailFragment;
    private CampaignDetailFragment campaignDetailFragment;
    private HomeFragment homeFragment;
    private WalkerFragment walkerFragment;
    private CodesFragment codesFragment;
    private CodesDetailFragment codesDetailFragment;
    private RedeemCodeFragment redeemCodeFragment;
    private static String VIDEOS_TAG="videos";
    private static String ZONES_TAG="zones";
    private static String CAMPAIGNS_TAG="campaigns";
    private static String WALKER_TAG="walker";
    private static String HOME_TAG="home";
    private static String CODES_TAG="codes";
    private Fragment currentFragment;
    FloatingActionButton fab;
    private NetworkManager networkManager;
    private SwipeRefreshLayout refreshLayout;
    User user;
    private List<Campaign> campaignList=new ArrayList<>();
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Publicity Walker");
        setSupportActionBar(toolbar);
        user = getUserFromDatabase();
        mayRequestLocation();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        networkManager = new NetworkManager();

        new Thread(new Runnable() {
            @Override
            public void run() {
                String userType = user.getType();
                if(userType.equals("client") || userType.equals("admin")) {
                    campaignsFragment = CampaignsFragment.newInstance(user, campaigListAdapterlistener);
                    adsFragment = AdsFragment.newInstance(user, fragmentListAdapterListener);
                    zonesFragment = ZonesFragment.newInstance(user, zoneListClickLstener);
                    zoneDetailFragment = ZoneDetailFragment.newInstance(user);
                    zoneDetailFragment.setZoneDetailListener(zoneDetailListener);
                    adDetailFragment = AdDetailFragment.newInstance(user, adsDetailListener);
                    campaignDetailFragment = campaignDetailFragment.newInstance(user, campaignDetailListener);
                    homeFragment = HomeFragment.newInstance(user, campaigListAdapterlistener);
                    codesFragment = CodesFragment.newInstance(user);
                    codesDetailFragment = CodesDetailFragment.newInstance(user, codesDetailFragmentListener);
                    redeemCodeFragment = RedeemCodeFragment.newInstance(user, networkManager);



                    networkManager.getCampaigns(user).enqueue(campaignsCallback);
                    networkManager.getAdsByCompany(user).enqueue(adsCallback);
                    networkManager.getZonesByCompany(user).enqueue(zonesCallback);
                    networkManager.getCodes(user).enqueue(codesCallback);
                    if(userType.equals("client")){
                        navMenu.findItem(R.id.admin_group).setVisible(false);
                        navMenu.findItem(R.id.walker_group).setVisible(false);
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,homeFragment).commit();
                }

                if(userType.equals("walker") || userType.equals("admin")){
                    walkerFragment = WalkerFragment.newInstance(HomeActivity.this);
                    if(userType.equals("walker")){
                        navMenu.findItem(R.id.admin_group).setVisible(false);
                        navMenu.findItem(R.id.client_group).setVisible(false);
                        getSupportFragmentManager().beginTransaction().replace(R.id.container,walkerFragment).commit();
                    }

                }


            }
        }).start();



        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(FloatingButtonClicked);

        fab.setVisibility(View.GONE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(drawerListener);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_home);
        TextView headerName = (TextView) headerView.findViewById(R.id.header_name);
        headerName.setText(user.getName());
        TextView headerEmail = (TextView) headerView.findViewById(R.id.header_email);
        headerEmail.setText(user.getEmail());

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.home_swipe_refresh);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(currentFragment!=null)
                if(currentFragment.getClass() == AdsFragment.class){
                    networkManager.getAdsByCompany(user).enqueue(adsCallback);
                }else if(currentFragment.getClass() == CampaignsFragment.class){
                    networkManager.getCampaigns(user).enqueue(campaignsCallback);
                }else if(currentFragment.getClass() == ZonesFragment.class){
                    networkManager.getZonesByCompany(user).enqueue(zonesCallback);
                }
            }
        });

    }

    private View.OnClickListener FloatingButtonClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            fab.setVisibility(View.GONE);
            String title = "Publicity Walker";
            if(currentFragment.getClass() == AdsFragment.class){
                currentFragment = adDetailFragment;
                getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment).commit();
                title = "Crear Anuncio";
            }else if(currentFragment.getClass() == CampaignsFragment.class){
                currentFragment = campaignDetailFragment;
                getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment).commit();
                title = "Crear Campaña";
            }else if(currentFragment.getClass() == ZonesFragment.class){
                currentFragment = zoneDetailFragment;
                getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment).commit();
                title="Crear Zonas";
            }else if(currentFragment.getClass() == CodesFragment.class){
                currentFragment = codesDetailFragment;
                getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment).commit();
                title="Crear Codigos";
            }
            getSupportActionBar().setTitle(title);
        }
    };

    private DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(View drawerView) {
            drawerView.bringToFront();

        }

        @Override
        public void onDrawerClosed(View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    private Callback<ApiResponse> activateCampaignCallback = new Callback<ApiResponse>() {
        @Override
        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
            Toast.makeText(HomeActivity.this,response.body().getMessage(),Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFailure(Call<ApiResponse> call, Throwable t) {

        }
    };

    private Callback<ApiResponse> campaignsCallback = new Callback<ApiResponse>() {
        @Override
        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                refreshLayout.setRefreshing(false);
            try {
                campaignList = response.body().getCampaignsList();
                homeFragment.setCampaignList(campaignList);
                campaignsFragment.setCampaignList(campaignList);
                campaignDetailFragment.setCampaignList(campaignList);


                if(currentFragment!=null)
                if(currentFragment.getClass() == CampaignDetailFragment.class){
                    currentFragment = campaignsFragment;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment).commit();
                    Toast.makeText(HomeActivity.this,getString(R.string.success_campaign_created),Toast.LENGTH_SHORT).show();
                    campaignDetailFragment = CampaignDetailFragment.newInstance(user,campaignDetailListener);
                    getSupportActionBar().setTitle("Campañas");
                }
                if(campaignList.size()>0) {
                    Campaign campaign = campaignList.get(0);
                    user.setPoints(campaign.getAvailablePoints());
                    saveUserToDatabase(user);
                }

            } catch (JSONException e) {
                Toast.makeText(HomeActivity.this,getString(R.string.error_general_networking),Toast.LENGTH_SHORT).show();
            } catch(NullPointerException e){

            }



        }

        @Override
        public void onFailure(Call<ApiResponse> call, Throwable t) {

        }
    };

    private Callback<ApiResponse> adsCallback = new Callback<ApiResponse>() {
        @Override
        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
            refreshLayout.setRefreshing(false);
            try {

                List<Ad> adList = response.body().getAdsList();
                adsFragment.setAdsList(adList);
                List<Ad> tempList = new ArrayList<>();
                tempList.add(new Ad(getString(R.string.campaign_ad_prompt)));
                tempList.addAll(adList);
                campaignDetailFragment.setAdList(tempList);

                if(currentFragment!=null)
                if(currentFragment.getClass() == AdDetailFragment.class){
                    currentFragment = adsFragment;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment).commit();
                    Toast.makeText(HomeActivity.this,getString(R.string.success_ad_created),Toast.LENGTH_SHORT).show();
                    adDetailFragment = AdDetailFragment.newInstance(user,adsDetailListener);
                    getSupportActionBar().setTitle("Anuncios");
                }
            } catch (Exception e) {
                Toast.makeText(HomeActivity.this,getString(R.string.error_general_networking),Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFailure(Call<ApiResponse> call, Throwable t) {

        }
    };
    private Callback<ApiResponse> codesCallback = new Callback<ApiResponse>() {
        @Override
        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
            refreshLayout.setRefreshing(false);
            try {
                List<Code> codeList = response.body().getCodesList();
                codesFragment.setCodeList(codeList);

                if(currentFragment!=null)
                    if(currentFragment.getClass() == CodesDetailFragment.class){
                        currentFragment = codesFragment;
                        getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment).commit();
                        Toast.makeText(HomeActivity.this,getString(R.string.success_code_created),Toast.LENGTH_SHORT).show();
                        codesDetailFragment = CodesDetailFragment.newInstance(user,codesDetailFragmentListener);
                        getSupportActionBar().setTitle("Codigos");
                    }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e){
                try {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
                    sharedPreferences.edit().remove("user").commit();
                    Toast.makeText(HomeActivity.this,response.errorBody().string(),Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(HomeActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }


        }

        @Override
        public void onFailure(Call<ApiResponse> call, Throwable t) {

        }
    };

    private Callback<ApiResponse> zonesCallback = new Callback<ApiResponse>() {
        @Override
        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
            refreshLayout.setRefreshing(false);
            try {
                List<Zone> zoneList = response.body().getZonesList();
                zonesFragment.setZoneList(zoneList);
                List<Zone> tempList = new ArrayList<>();
                tempList.add(new Zone(getString(R.string.campaign_zone_prompt)));
                tempList.addAll(zoneList);
                campaignDetailFragment.setZoneList(tempList);

                if(currentFragment!=null)
                if(currentFragment.getClass() == ZoneDetailFragment.class){
                    currentFragment = zonesFragment;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment).commit();
                    Toast.makeText(HomeActivity.this,getString(R.string.success_zone_created),Toast.LENGTH_SHORT).show();
                    zoneDetailFragment = ZoneDetailFragment.newInstance(user);
                    getSupportActionBar().setTitle("Zonas");
                }


            } catch (Exception e) {
                Toast.makeText(HomeActivity.this,getString(R.string.error_general_networking),Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

        }

        @Override
        public void onFailure(Call<ApiResponse> call, Throwable t) {

        }
    };


    private User getUserFromDatabase(){
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String userString = sharedPreferences.getString("user",null);
        User user = null;
        if(userString!=null)
            user = gson.fromJson(userString,User.class);

        return user;
    }

    private void saveUserToDatabase(User user){
        Gson gson = new Gson();
        String userString = gson.toJson(user);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        editor.putString("user",userString);
        editor.commit();
    }

    private ZoneListAdapter.ZoneListClickLstener zoneListClickLstener = new ZoneListAdapter.ZoneListClickLstener() {
        @Override
        public void OnZoneClicked(Zone zone) {
            Intent intent = new Intent(HomeActivity.this,ZoneCreatorActivity.class);
            intent.putExtra("zone",zone);
            startActivity(intent);
        }
    };

    private ZoneDetailFragment.ZoneDetailListener zoneDetailListener = new ZoneDetailFragment.ZoneDetailListener() {
        @Override
        public void OnCreateNewZone(Zone zone) {
              networkManager.saveZone(zone,getUserFromDatabase()).enqueue(zonesCallback);
        }
    };

    private AdDetailFragment.AdsDetailListener adsDetailListener = new AdDetailFragment.AdsDetailListener() {
        @Override
        public void OnCreateNewAd(Ad ad) {
        sendForm(ad);
        }
    };

    private CampaignDetailFragment.CampaignDetailListener campaignDetailListener = new CampaignDetailFragment.CampaignDetailListener() {
        @Override
        public void OnCreateNewCampaign(Campaign campaign) {
            networkManager.saveCampaign(campaign,getUserFromDatabase()).enqueue(campaignsCallback);
        }
    };

    private CampaignListAdapter.CampaigListAdapterlistener campaigListAdapterlistener = new CampaignListAdapter.CampaigListAdapterlistener() {
        @Override
        public void OnCheckedSwitchChanged(Campaign campaign) {
            networkManager.updateCampaign(campaign,getUserFromDatabase()).enqueue(activateCampaignCallback);
        }
    }  ;

    private FragmentListAdapter.FragmentListAdapterListener fragmentListAdapterListener = new FragmentListAdapter.FragmentListAdapterListener() {
        @Override
        public void onAdCicked(Ad ad) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(ad.getAdURL()), "video/3gpp");
            startActivity(Intent.createChooser(intent, "Reproducir Anuncio"));

        }
    };

    private CodesDetailFragment.CodesDetailFragmentListener codesDetailFragmentListener = new CodesDetailFragment.CodesDetailFragmentListener() {
        @Override
        public void createCode(Code code) {
            networkManager.saveCode(code,user).enqueue(codesCallback);
        }
    };

    private void sendForm(Ad ad){
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("video/mp4"),
                        ad.getVideoFile()
                );

        MultipartBody.Part videoFile=MultipartBody.Part.createFormData("video_file",ad.getVideoFile().getName(),requestFile);

        RequestBody requestFile2 =
                RequestBody.create(
                        MediaType.parse("image  /jpeg"),
                        ad.getThumbnailFile()
                );

        MultipartBody.Part thumbnailFile=MultipartBody.Part.createFormData("thumbnail_file",ad.getThumbnailFile().getName(),requestFile2);


        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), ad.getName());
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), ad.getDescription());
        RequestBody idCompany = RequestBody.create(MediaType.parse("text/plain"), getUserFromDatabase().getCompanyId()+"");
        RequestBody duration = RequestBody.create(MediaType.parse("text/plain"), ad.getDuration()+"");

        networkManager.saveAd(videoFile,thumbnailFile,name,description,idCompany,duration,getUserFromDatabase().getToken()).enqueue(adsCallback);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.home, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String title="Publicity Walker";
        fab.setVisibility(View.VISIBLE);
        if (id == R.id.nav_campaigns) {
            currentFragment = campaignsFragment;
            getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment,CAMPAIGNS_TAG).commit();
            title = "Campañas";

        } else if (id == R.id.nav_videos) {
            currentFragment = adsFragment;
            getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment,VIDEOS_TAG).commit();
            title="Anuncios";

        } else if (id == R.id.nav_zonas) {
            currentFragment = zonesFragment;
            getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment,ZONES_TAG).commit();
            title = "Zonas";

        } else if (id == R.id.nav_walker) {

            currentFragment = walkerFragment;
            getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment,WALKER_TAG).commit();
            title="Walker";
            fab.setVisibility(View.GONE);

        } else if (id == R.id.nav_home) {
           /* currentFragment = homeFragment;
            getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment,HOME_TAG).commit();
            homeFragment.setCampaignList(campaignList);
            homeFragment.updateMap();*/
           Intent intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_logout){
            PreferenceManager.getDefaultSharedPreferences(this).edit().clear().apply();
            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }else if (id == R.id.nav_codes){
            currentFragment = codesFragment;
            getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment,CODES_TAG).commit();
            title="Codigos";
        }else if (id == R.id.nav_redeem){
            currentFragment = redeemCodeFragment;
            getSupportFragmentManager().beginTransaction().replace(R.id.container,currentFragment,CODES_TAG).commit();
            title="Redimir Código";
            fab.setVisibility(View.GONE);
        }
        getSupportActionBar().setTitle(title);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        return true;
    }
    private boolean mayRequestLocation() {

        if (checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
            Snackbar.make(fab, R.string.permission_rationale_location, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{ACCESS_FINE_LOCATION}, 322);
                        }
                    });
        } else {
            requestPermissions(new String[]{ACCESS_FINE_LOCATION}, 322);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //requestPermissions();
        }
    }
}
