package com.publicitywalker.pwclient.Activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.Loader;
import android.database.Cursor;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.publicitywalker.pwclient.Model.ApiResponse;
import com.publicitywalker.pwclient.Model.Company;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Networking.NetworkManager;
import com.publicitywalker.pwclient.R;

import org.json.JSONException;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor>, Callback<ApiResponse> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    private static final int REQUEST_ACCESS_LOCATION=1;
    private static final int REQUEST_ACESS_FILES=2;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */


    // UI references.
    private AutoCompleteTextView mEmailView,newUserName,newUserEmail,newCompanyName,newCompanyDescription,newCompanyEmail,newcompanyCellphone,newCompanyTelephone,newCompanyAddress,newCompanyWebsite;
    private EditText mPasswordView,newUserPassword,newUserOtherPassword;
    private View mProgressView;
    private View mLoginFormView;
    private LinearLayout loginLine,newUserLine;
    private TextView newUserLink,loginLink;
    private NetworkManager networkManager;
    private SharedPreferences sharedPreferences;
    private Button newUserButton;
    private Typeface roboto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        roboto = Typeface.createFromAsset(getAssets(),  "fonts/Roboto.ttf");
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mEmailView.setTypeface(roboto);
        requestPermissions();


        networkManager = new NetworkManager();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setTypeface(roboto);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        loginLine = (LinearLayout) findViewById(R.id.email_login_form);
        newUserLine = (LinearLayout) findViewById(R.id.new_user_line);
        newUserLink = (TextView)  findViewById(R.id.new_account_link);
        loginLink = (TextView) findViewById(R.id.emali_login_link);

        newUserName = (AutoCompleteTextView) findViewById(R.id.new_user_name);
        newUserName.setTypeface(roboto);
        newUserEmail = (AutoCompleteTextView) findViewById(R.id.new_user_email);
        newUserEmail.setTypeface(roboto);
        newUserPassword = (EditText) findViewById(R.id.new_user_password);
        newUserPassword.setTypeface(roboto);
        newUserOtherPassword = (EditText) findViewById(R.id.new_user_other_password);
        newUserOtherPassword.setTypeface(roboto);
        newCompanyName = (AutoCompleteTextView) findViewById(R.id.new_company_name);
        newCompanyName.setTypeface(roboto);
        newCompanyDescription = (AutoCompleteTextView) findViewById(R.id.new_company_description);
        newCompanyDescription.setTypeface(roboto);
        newCompanyEmail = (AutoCompleteTextView) findViewById(R.id.new_company_email);
        newCompanyEmail.setTypeface(roboto);
        newcompanyCellphone = (AutoCompleteTextView) findViewById(R.id.new_company_cellphone);
        newcompanyCellphone.setTypeface(roboto);
        newCompanyTelephone = (AutoCompleteTextView) findViewById(R.id.new_company_telephone);
        newCompanyTelephone.setTypeface(roboto);
        newCompanyAddress = (AutoCompleteTextView) findViewById(R.id.new_company_address);
        newCompanyAddress.setTypeface(roboto);
        newCompanyWebsite = (AutoCompleteTextView) findViewById(R.id.new_company_website);
        newCompanyWebsite.setTypeface(roboto);
        newUserButton = (Button) findViewById(R.id.sign_up_button);
        newUserButton.setTypeface(roboto);

        newUserLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginLine.setVisibility(View.GONE);
                newUserLine.setVisibility(View.VISIBLE);
            }
        });

        loginLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginLine.setVisibility(View.VISIBLE);
                newUserLine.setVisibility(View.GONE);

            }
        });

        newUserButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressView.setVisibility(View.VISIBLE);
                createNewUser();
            }
        });

        User user = getUserFromDatabase();
        if(user!=null)
            launchHomeActivity();
    }

    private void createNewUser(){
        if (newUserName.getText().toString().length()==0){
            newUserName.setError(getString(R.string.error_empty_field));
        }else if(newUserEmail.getText().toString().length()==0)
            newUserEmail.setError(getString(R.string.error_empty_field));
        else if(newUserPassword.getText().toString().length()==0)
            newUserPassword.setError(getString(R.string.error_empty_field));
        else if(newUserOtherPassword.getText().toString().length()==0)
            newUserOtherPassword.setError(getString(R.string.error_empty_field));
        else if(newCompanyName.getText().toString().length()==0)
            newCompanyName.setError(getString(R.string.error_empty_field));
        else if(newCompanyDescription.getText().toString().length()==0)
            newCompanyDescription.setError(getString(R.string.error_empty_field));
        else if(newCompanyEmail.getText().toString().length()==0)
            newCompanyEmail.setError(getString(R.string.error_empty_field));
        else if(newcompanyCellphone.getText().toString().length()==0)
            newcompanyCellphone.setError(getString(R.string.error_empty_field));
        else if(newCompanyTelephone.getText().toString().length()==0)
            newCompanyTelephone.setError(getString(R.string.error_empty_field));
        else if(newCompanyAddress.getText().toString().length()==0)
            newCompanyAddress.setError(getString(R.string.error_empty_field));
        else if(!newUserPassword.getText().toString().equals(newUserOtherPassword.getText().toString()))
            newUserOtherPassword.setError(getString(R.string.error_passwords_not_same));
        else if(!newUserEmail.getText().toString().contains("@") || !newUserEmail.getText().toString().contains("."))
            newUserEmail.setError(getString(R.string.error_invalid_email));
        else if(!newCompanyEmail.getText().toString().contains("@") || !newCompanyEmail.getText().toString().contains("."))
            newCompanyEmail.setError(getString(R.string.error_invalid_email));
        else{
            Company company = new Company(newCompanyName.getText().toString(),newCompanyDescription.getText().toString(),newCompanyEmail.getText().toString(),newCompanyTelephone.getText().toString(),newcompanyCellphone.getText().toString(),newCompanyWebsite.getText().toString(),newCompanyAddress.getText().toString());
            networkManager.createCompany(company).enqueue(CompanyCreationListener);
        }
    }

    private void launchHomeActivity(){
        Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }


    private boolean requestPermissions(){
        if(!mayRequestFiles()){
            return false;
        }

        getLoaderManager().initLoader(0,null,this);
        return true;
    }




    private boolean mayRequestFiles() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE) || shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
            Snackbar.make(mEmailView, R.string.permission_rationale_files, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACESS_FILES);
                        }
                    });
        } else {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACESS_FILES);
        }
        return false;
    }

    private User getUserFromDatabase(){
        Gson gson = new Gson();
        String userString = sharedPreferences.getString("user",null);
        User user = null;
        if(userString!=null)
            user = gson.fromJson(userString,User.class);

        return user;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //requestPermissions();
            }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mProgressView.setVisibility(View.VISIBLE);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
          //  showProgress(true);
           // mAuthTask = new UserLoginTask(email, password);
           // mAuthTask.execute((Void) null);
            networkManager.login(new User(email,password)).enqueue(LoginActivity.this);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return null;/*new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");*/
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
      /*  List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);*/
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    @Override
    public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {

        mProgressView.setVisibility(View.GONE);
        if(response.body().getType() == ApiResponse.Type.SUCCESS) {
            try {
                User user = response.body().getUser();
                Gson gson = new Gson();
                String userString = gson.toJson(user);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("user",userString);
                editor.commit();

              launchHomeActivity();
                //TODO login successful
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            mPasswordView.setError(getString(R.string.error_authentication_failed));            //Login no fue succesful
        }
    }

    private Callback<ApiResponse> CompanyCreationListener = new Callback<ApiResponse>() {
        @Override
        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
            ApiResponse apiResponse = response.body();
            if(apiResponse!=null)
            if(apiResponse.getType() == ApiResponse.Type.SUCCESS){
                try {
                    Company company = apiResponse.getCompany();
                    User user = new User(newUserName.getText().toString(),newUserEmail.getText().toString(),newUserPassword.getText().toString(),company.getId());
                    networkManager.createUser(user).enqueue(UserCreationListener);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        @Override
        public void onFailure(Call<ApiResponse> call, Throwable t) {

            mProgressView.setVisibility(View.GONE);
        }
    };

    private Callback<ApiResponse> UserCreationListener = new Callback<ApiResponse>() {
        @Override
        public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
            mProgressView.setVisibility(View.GONE);
            ApiResponse apiResponse = response.body();
            if(apiResponse!=null)
            if(apiResponse.getType()== ApiResponse.Type.SUCCESS){
                //Usuario creado satisfactoriamente
                try {
                    User user = apiResponse.getUser();
                    user.setPassword(newUserPassword.getText().toString());
                    networkManager.login(user).enqueue(LoginActivity.this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        @Override
        public void onFailure(Call<ApiResponse> call, Throwable t) {

        }
    };

    @Override
    public void onFailure(Call<ApiResponse> call, Throwable t) {

    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
}

