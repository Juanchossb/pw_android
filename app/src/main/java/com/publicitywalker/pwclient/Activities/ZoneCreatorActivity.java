package com.publicitywalker.pwclient.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;
import com.publicitywalker.pwclient.Model.Zone;
import com.publicitywalker.pwclient.R;

import java.util.List;

public class ZoneCreatorActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Zone zone;
    private TextView saveButton;
    private ImageView deleteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_creator);

        zone = (Zone) getIntent().getSerializableExtra("zone");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        saveButton = (TextView) findViewById(R.id.creator_save_button);
        deleteButton = (ImageView) findViewById(R.id.creator_delete_button);
        if (zone == null)
            deleteButton.setVisibility(View.GONE);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        // Add a marker in Sydney and move the camera
        if(zone!=null) {
            Polygon polygon = googleMap.addPolygon(zone.getPolygon(ZoneCreatorActivity.this));
            int padding = 0; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(zone.getMapBounds(), padding);
            googleMap.moveCamera(cu);
        }

        final PolygonOptions polygonOptions = new PolygonOptions();

        polygonOptions.fillColor(getColor(R.color.colorAlphaPrimary));
        polygonOptions.strokeColor(getColor(R.color.colorAccent));
        polygonOptions.strokeWidth(2);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                polygonOptions.add(latLng);
                mMap.clear();
                mMap.addPolygon(polygonOptions);
                deleteButton.setVisibility(View.VISIBLE);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<LatLng> list= polygonOptions.getPoints();

                    list.remove(list.size() - 1);
                    PolygonOptions tempoly = new PolygonOptions();
                    mMap.clear();
                    mMap.addPolygon(polygonOptions);
                if (list.size()==0) {
                    deleteButton.setVisibility(View.GONE);
                }
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",gson.toJson(polygonOptions.getPoints()));
                setResult(1,returnIntent);
                finish();
            }
        });


        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);

        if(location!=null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 14);
            mMap.animateCamera(yourLocation);
        }


        }
}
