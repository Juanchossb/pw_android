package com.publicitywalker.pwclient.Model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancho on 25/07/17.
 */

public class ApiResponse {
    public enum Type{
        SUCCESS,WARNING
    }

    @SerializedName("type")
    private String type;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private Object data;

    @SerializedName("errorBody")
    private Error error;

    public User getUser() throws JSONException {
        Gson gson = new Gson();
        String jsonObject = gson.toJson(data);
        JSONObject obj = new JSONObject(jsonObject);
        return gson.fromJson(obj.toString(),User.class);
    }

    public Company getCompany() throws JSONException {
        Gson gson = new Gson();
        return gson.fromJson(getGenericDataObject(gson).toString(),Company.class);
    }


    private JSONObject getGenericDataObject(Gson gson) throws JSONException {
        String jsonObject = gson.toJson(data);
        return new JSONObject(jsonObject);
    }

    private JSONArray getGenericDataArray(Gson gson) throws JSONException {
        String jsonObject = gson.toJson(data);
        return new JSONArray(jsonObject);
    }

    public Type getType(){
        if(type.equals("success"))
            return Type.SUCCESS;
        else
            return Type.WARNING;
    }

    public List<Campaign> getCampaignsList() throws JSONException {
        Gson gson = new Gson();
        List<Campaign> campaignList = new ArrayList<>();
        JSONArray jsonArray = null;
       try{
            JSONObject object = getGenericDataObject(gson);
            jsonArray = object.getJSONArray("data");
        }catch (JSONException e){
           jsonArray = getGenericDataArray(gson);
       }

        for(int i=0;i< jsonArray.length();i++){
            Campaign campaign = gson.fromJson(jsonArray.getJSONObject(i).toString(),Campaign.class);
            campaignList.add(campaign);
        }
        return  campaignList;
    }

    public List<Ad> getAdsList() throws JSONException {
        Gson gson = new Gson();
        List<Ad> adList = new ArrayList<>();
       // JSONObject object = getGenericDataObject(gson);
        JSONArray jsonArray=getGenericDataArray(gson);
        for(int i=0;i< jsonArray.length();i++){
            Ad ad = gson.fromJson(jsonArray.getJSONObject(i).toString(),Ad.class);
            adList.add(ad);
        }
        return  adList;
    }

    public List<Zone> getZonesList() throws JSONException {
        Gson gson = new Gson();
        List<Zone> zoneList = new ArrayList<>();
        //JSONObject object = getGenericDataObject(gson);
        JSONArray jsonArray=getGenericDataArray(gson);
        for(int i=0;i< jsonArray.length();i++){
            Zone zone = gson.fromJson(jsonArray.getJSONObject(i).toString(),Zone.class);
            zoneList.add(zone);
        }
        return  zoneList;
    }

    public List<Code> getCodesList() throws JSONException {
        Gson gson = new Gson();
        List<Code> codesList = new ArrayList<>();
        //JSONObject object = getGenericDataObject(gson);
        JSONArray jsonArray=getGenericDataArray(gson);
        for(int i=0;i< jsonArray.length();i++){
            Code code = gson.fromJson(jsonArray.getJSONObject(i).toString(),Code.class);
            codesList.add(code);
        }
        return  codesList;
    }

    public boolean hasErrors(){
        return error!=null;
    }

    public Error getError(){
        return error;
    }

    public String getMessage(){
        return message;
    }

}
