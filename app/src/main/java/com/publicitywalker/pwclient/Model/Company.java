package com.publicitywalker.pwclient.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by juancho on 25/07/17.
 */

public class Company {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("email")
    private String email;
    @SerializedName("telephone")
    private String telefone;
    @SerializedName("cellphone")
    private String cellphone;
    @SerializedName("urlwebpage")
    private String webpage;
    @SerializedName("address")
    private String address;

    public Company(String name, String description, String email, String telephone, String cellphone, String webpage, String address){
        this.name = name;
        this.description = description;
        this.email = email;
        this.telefone = telephone;
        this.cellphone = cellphone;
        this.webpage = webpage;
        this.address = address;
    }

    public int getId(){
        return id;
    }

}
