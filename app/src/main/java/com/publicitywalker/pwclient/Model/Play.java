package com.publicitywalker.pwclient.Model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

/**
 * Created by juancho on 11/08/17.
 */

public class Play {
    @SerializedName("id_walker")
    private int idWalker;
    @SerializedName("id_campaign")
    private int idCampaign;
    @SerializedName("points_consumed")
    private float pointsConsumed;
    @SerializedName("location")
    private String location;

    public float getPointsConsumed(){
        return pointsConsumed;
    }

    public LatLng getLatLng(){
        String loc = location.replace("POINT(","").replace(")","");
        String[] temLoc = loc.split(" ");
        Double lat = Double.parseDouble(temLoc[0]);
        Double lng = Double.parseDouble(temLoc[1]);
        LatLng result = new LatLng(lat,lng);
        return result;
    }
}
