package com.publicitywalker.pwclient.Model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by juancho on 12/08/17.
 */

public class MarkerCluster implements ClusterItem {

    private  LatLng mPosition;
    private  String mTitle;
    private  String mSnippet;

    public MarkerCluster(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public MarkerCluster(double lat, double lng, String title, String snippet) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        mSnippet = snippet;
    }
    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }
}
