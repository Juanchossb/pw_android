package com.publicitywalker.pwclient.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by juancho on 26/07/17.
 */

public class Campaign {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("investment")
    private double investment;
    @SerializedName("company_id")
    private int companyId;
    @SerializedName("created_at")
    private String creationDate;
    @SerializedName("priority")
    private int priority;
    @SerializedName("frequency")
    private int frequency;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("zone_list")
    private List<Zone> zoneList;
    @SerializedName("ad_list")
    private List<Ad> adList;
    @SerializedName("active")
    private int active;
    @SerializedName("plays")
    private List<Play> playList;
    @SerializedName("points_available")
    private List<AvailablePoint> pointList;


    private static String THUMBNAIL_URL="http://app.publicitywalker.com/storage/thumbnails/";

    public Campaign(String name, String description, double investment,int priority,int frequency,List<Zone> zoneList, List<Ad> adList ,int companyId,String thumbnail){
        this.name = name;
        this.description = description;
        this.investment = investment;
        this.priority = priority;
        this.frequency = frequency;
        this.companyId = companyId;
        this.zoneList = zoneList;
        this.adList = adList;
        this.thumbnail = thumbnail;
    }

    public int getId(){
        return id;
    }
    public String getName(){
        return  name;
    }

    public String getDescription(){
        return description;
    }

    public String getThumbnail(){
        if(!thumbnail.startsWith("http"))
        return String.format("%s%s",THUMBNAIL_URL,thumbnail);
        else
            return thumbnail;
    }

    public String getCreationDate(){
        return creationDate;
    }

    public boolean isActive(){
        return active == 1;
    }

    public void setActive(Boolean active){
        if(active)
        this.active = 1;
        else
            this.active=0;
    }

    public int getNumberOfAdsPlayed(){
        return playList.size();
    }

    public float getPointsConsumed(){
        float result=0;
        for(Play play: playList)
            result+=play.getPointsConsumed();
        return result;
    }

    public double getInvestment(){
        return investment;
    }

    public double getPortcetageConsumed(){
        return (getPointsConsumed()*100)/getInvestment();
    }

    public double getAvailablePoints(){
        return pointList.get(0).points;
    }

    public List<Play> getPlayList(){
        return playList;
    }

    private class AvailablePoint{
        @SerializedName("points")
        public double points;
    }
}
