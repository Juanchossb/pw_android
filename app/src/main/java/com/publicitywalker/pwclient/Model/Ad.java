package com.publicitywalker.pwclient.Model;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Created by juancho on 27/07/17.
 */

public class Ad {

    @SerializedName("id")
    private int id;
    @SerializedName("description")
    private String description;
    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String video;
    @SerializedName("campaign_id")
    private int idCampaign;
    @SerializedName("audio")
    private String audio;
    @SerializedName("area")
    private float area;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("id_company")
    private int idCompany;
    @SerializedName("video_file")
    private File video_file;
    @SerializedName("thumbnail_file")
    private File thumbnail_file;
    @SerializedName("duration")
    private int duration;


    private static String THUMBNAIL_DIRECTORY="http://app.publicitywalker.com/storage/thumbnails/";
    private static String AD_DIRECTORY="http://app.publicitywalker.com/storage/ads/";

    public Ad (String name, String description,int idCompany, File video_file, File thumbnail_file, int duration){
        this.name = name;
        this.description = description;
        this.idCampaign = idCompany;
        this.video_file = video_file;
        this.thumbnail_file = thumbnail_file;
        this.duration = duration;
    }
    public Ad (String name){
        this.name = name;
    }


    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public float getArea(){
        return area;
    }

    public String getThumbnail(){
        return String.format("%s%s",THUMBNAIL_DIRECTORY,thumbnail);
    }
    public String getAdURL(){
        return String.format("%s%s",AD_DIRECTORY,video);
    }

    public File getVideoFile(){
        return video_file;
    }

    public File getThumbnailFile(){
        return thumbnail_file;
    }

    public int getId(){
        return id;
    }

    public int getDuration(){
        return duration;
    }
    public String getDurationsInMins(){
        return String.format("%d min, %d seg",
                TimeUnit.MILLISECONDS.toMinutes(duration*1000),
                TimeUnit.MILLISECONDS.toSeconds(duration*1000) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration*1000))
        );
    }

    @Override
    public String toString() {
        return name;
    }
}
