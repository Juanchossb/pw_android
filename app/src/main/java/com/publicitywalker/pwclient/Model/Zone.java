package com.publicitywalker.pwclient.Model;

import android.content.Context;
import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.annotations.SerializedName;
import com.publicitywalker.pwclient.R;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by juancho on 28/07/17.
 */

public class Zone implements Serializable{
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("company_id")
    private int idCompany;
    @SerializedName("keywords")
    private String keywords;
    @SerializedName("location")
    private String rawLocation;
    @SerializedName("area")
    private float area;
    @SerializedName("id_company")
    private int idc = idCompany;



    public Zone(String name, String description, int idCompany, String rawLocation){
        this.name = name;
        this.description=description;
        this.idCompany = idCompany;
        this.idc = idCompany;
        this.rawLocation = rawLocation;
    }

    public Zone(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getFormattedArea(){
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        return df.format(area);
    }
    private String formatLocation(){
        return rawLocation.replace("POLYGON((","").replace("))","");
    }
    public PolygonOptions getPolygon(Context context){
        String [] coordinates = formatLocation().split(",");
        PolygonOptions polygonOptions = new PolygonOptions();
        for(String coordiante : coordinates){
            String latlng []= coordiante.split(" ");
            polygonOptions.add(new LatLng(Double.parseDouble(latlng[0]),Double.parseDouble(latlng[1])));
        }
       polygonOptions.fillColor(context.getColor(R.color.colorAlphaPrimary));
        polygonOptions.strokeColor(context.getColor(R.color.colorAccent));
        polygonOptions.strokeWidth(2);
        return polygonOptions;
    }
    public String getRawLocation(){
        return rawLocation;
    }

    public LatLngBounds getMapBounds(){
        String [] coordinates = formatLocation().split(",");
        final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(String coordiante : coordinates){
            String latlng []= coordiante.split(" ");
            builder.include(new LatLng(Double.parseDouble(latlng[0]),Double.parseDouble(latlng[1])));
        }

        return builder.build();
    }

    @Override
    public String toString() {
        return name;
    }
}
