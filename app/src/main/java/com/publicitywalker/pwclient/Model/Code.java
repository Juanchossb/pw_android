package com.publicitywalker.pwclient.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by juancho on 11/09/17.
 */

public class Code {
    @SerializedName("id")
    private int id;
    @SerializedName("code")
    private String code;
    @SerializedName("amount")
    private int amount;
    @SerializedName("user_name")
    private String userName;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("comapny_address")
    private String companyAddress;
    @SerializedName("phone")
    private String phone;
    @SerializedName("active")
    private int active;
    @SerializedName("notes")
    private String notes;

    public Code(String code, int amount, String userName, String companyName, String companyAddress, String phone, String notes){
        this.code=code;
        this.amount=amount;
        this.userName=userName;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.phone=phone;
        this.notes=notes;
        this.active=1;
    }

    public String getCode(){
        return code;
    }

    public int getAmount(){
        return amount;
    }

    public String getUserName(){
        return userName;
    }

    public String getCompanyName(){
        return companyName;
    }

    public String getPhone(){
        return phone;
    }

    public boolean isActive(){
        return active==1;
    }

    public String getNotes(){
        return notes;
    }
}
