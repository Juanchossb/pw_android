package com.publicitywalker.pwclient.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by juancho on 25/07/17.
 */

public class User implements Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("active")
    private int active;

    @SerializedName("token")
    private String token;

    @SerializedName("password")
    private String password;

    @SerializedName("id_company")
    private int idCompany;

    @SerializedName("points")
    private double points;

    @SerializedName("type")
    private String type;

    public User(String email, String password){
        this.email = email;
        this.password = password;
    }

    public User (String name, String email, String password, int companyId){
        this.name = name;
        this.email = email;
        this.password = password;
        this.idCompany = companyId;
        this.type = "client";
    }

    public String getEmail(){
        return email;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public int getCompanyId(){
        return idCompany;
    }

    public String getToken(){
        return token;
    }

    public double getPoints(){
        return points;
    }

    public void setPoints(double points){
        this.points = points;
    }

    public String getType(){
        return type;
    }

    public String getName(){
        return name;
    }
}
