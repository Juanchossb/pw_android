package com.publicitywalker.pwclient.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.R;

/**
 * Created by juancho on 10/08/17.
 */

public class CampaignListHolder extends RecyclerView.ViewHolder {
    public TextView title;
    public TextView description;
    public TextView subtitle;
    public Switch rowSwitch;
    public ImageView image;
    public Campaign campaign;
    public TextView played;
    public ProgressBar progressBar;
    public TextView porcentage;
    public TextView amount;
    public CampaignListHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.row_title);
        description = (TextView) itemView.findViewById(R.id.row_description);
        subtitle = (TextView) itemView.findViewById(R.id.row_subtitle);
        rowSwitch = (Switch) itemView.findViewById(R.id.row_switch);
        image = (ImageView) itemView.findViewById(R.id.row_image);
        played = (TextView) itemView.findViewById(R.id.row_played);
        progressBar = (ProgressBar) itemView.findViewById(R.id.row_progress_bar);
        porcentage = (TextView) itemView.findViewById(R.id.row_porcentage);
        amount = (TextView) itemView.findViewById(R.id.row_amount);
        this.setIsRecyclable(false);
    }
}
