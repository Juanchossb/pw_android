package com.publicitywalker.pwclient.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;
import com.publicitywalker.pwclient.Model.Zone;
import com.publicitywalker.pwclient.R;

/**
 * Created by juancho on 28/07/17.
 */

public class ZoneListHolder extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView description;
    public TextView subtitle;
    public TextView subtext;
    public MapView mapView;
    public Zone zone;
    public LinearLayout parent;
    public ImageView delete;

    public ZoneListHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.row_title);
        description = (TextView) itemView.findViewById(R.id.row_description);
        subtitle = (TextView) itemView.findViewById(R.id.row_subtitle);
        subtext = (TextView) itemView.findViewById(R.id.row_subtext);
        mapView = (MapView) itemView.findViewById(R.id.row_map);
        parent = (LinearLayout) itemView.findViewById(R.id.row_parent);
        delete = (ImageView) itemView.findViewById(R.id.row_delete);
    }
}
