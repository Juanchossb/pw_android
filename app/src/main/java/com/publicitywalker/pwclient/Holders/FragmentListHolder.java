package com.publicitywalker.pwclient.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.publicitywalker.pwclient.R;

/**
 * Created by juancho on 26/07/17.
 */

public class FragmentListHolder extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView description;
    public TextView subtitle;
    public TextView subtext;
    public ImageView image;
    public ImageView delete;
    public LinearLayout line;
    public FragmentListHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(R.id.row_title);
        description = (TextView) itemView.findViewById(R.id.row_description);
        subtitle = (TextView) itemView.findViewById(R.id.row_subtitle);
        subtext = (TextView) itemView.findViewById(R.id.row_subtext);
        image = (ImageView) itemView.findViewById(R.id.row_image);
        delete = (ImageView) itemView.findViewById(R.id.row_delete);
        line = (LinearLayout) itemView.findViewById(R.id.row_line);

    }
}
