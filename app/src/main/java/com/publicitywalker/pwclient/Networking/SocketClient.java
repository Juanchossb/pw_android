package com.publicitywalker.pwclient.Networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by juancho on 11/09/17.
 */

public class SocketClient {

    SocketClientListener listener;

    public SocketClient(SocketClientListener listener){
        this.listener = listener;
    }
    private class SocketClientThread extends Thread{

        @Override
        public void run() {
            Socket socket = null;

            try {
                socket = new Socket(InetAddress.getLocalHost(),SocketServer.SocketServerPORT);

                PrintStream printStream = new PrintStream(socket.getOutputStream());
                printStream.print("checking connection");
                socket.close();
                if(listener!=null)
                    listener.sockerServerConnection(true);
            } catch (IOException e) {
                if(listener!=null)
                    listener.sockerServerConnection(false);
            }
        }
    }

    public void launchClient(){
        Thread socketThread = new SocketClientThread();
        socketThread.start();
    }
    public interface SocketClientListener{
        void sockerServerConnection(boolean isConnected);
    }
}
