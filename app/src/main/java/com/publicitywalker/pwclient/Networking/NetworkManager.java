package com.publicitywalker.pwclient.Networking;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import com.google.gson.internal.Excluder;
import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.ApiResponse;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.Code;
import com.publicitywalker.pwclient.Model.Company;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Model.Zone;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by juancho on 25/07/17.
 */

public class NetworkManager {

    private static String BASE_URL="http://app.publicitywalker.com/api/";
    private NetworkApi networkApi;

    public NetworkManager(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        networkApi = retrofit.create(NetworkApi.class);
    }

    public Call<ApiResponse> login(User user){
        return networkApi.login(user);
    }

    public Call<ApiResponse> createCompany(Company company){
        return networkApi.createCompany(company);
    }

    public Call<ApiResponse> createUser(User user){
        return networkApi.createUser(user);
    }

    public Call<ApiResponse> getCampaigns(User user){
        return networkApi.getCampaigns(user.getCompanyId(),String.format("Bearer %s",user.getToken()));
    }

    public  Call<ApiResponse> getAdsByCompany(User user){
        return networkApi.getAdsByCompany(user.getCompanyId(),String.format("Bearer %s",user.getToken()));
    }

    public Call<ApiResponse> getZonesByCompany(User user){
        return networkApi.getZonesByComapany(user.getCompanyId(),String.format("Bearer %s",user.getToken()));
    }

    public Call<ApiResponse> saveZone(Zone zone, User user){
        return networkApi.saveZone(zone,String.format("Bearer %s",user.getToken()));
    }

    public Call<ApiResponse> saveAd(MultipartBody.Part videoFile, MultipartBody.Part thumbnailFile, RequestBody name, RequestBody description, RequestBody companyId, RequestBody duration, String token){
        return networkApi.saveAd(videoFile,thumbnailFile,name,description,companyId,duration,String.format("Bearer %s",token));
    }

    public Call<ApiResponse> saveCampaign(Campaign campaign, User user){
        return networkApi.saveCampaign(campaign,String.format("Bearer %s",user.getToken()));
    }

    public Call<ApiResponse> updateCampaign(Campaign campaign, User user){
        return networkApi.updateCampaign(campaign.getId(),campaign,String.format("Bearer %s",user.getToken()));
    }

    public Call<ApiResponse> getCodes(User user){
        return networkApi.getCodes(String.format("Bearer %s",user.getToken()));
    }

    public Call<ApiResponse> saveCode(Code code, User user){
            return  networkApi.saveCode(code,String.format("Bearer %s",user.getToken()));
    }

    public Call<ApiResponse> redeemCode(String code, User user){
        return networkApi.redeemCode(user.getCompanyId(),code,String.format("Bearer %s",user.getToken()));
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
}
