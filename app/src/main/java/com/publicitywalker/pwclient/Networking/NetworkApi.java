package com.publicitywalker.pwclient.Networking;

import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.ApiResponse;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.Code;
import com.publicitywalker.pwclient.Model.Company;
import com.publicitywalker.pwclient.Model.User;
import com.publicitywalker.pwclient.Model.Zone;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by juancho on 25/07/17.
 */

public interface NetworkApi {
    @POST("v1/login")
    Call<ApiResponse> login(@Body User user);

    @POST("prototype/createcompany")
    Call<ApiResponse> createCompany(@Body Company company);

    @POST("prototype/createuser")
    Call<ApiResponse> createUser(@Body User user);

    @GET("v1/campaign/{company_id}")
    Call<ApiResponse> getCampaigns(@Path("company_id") int companyId, @Header("Authorization") String token);

    @GET("v1/adsbycompany/{id_company}")
    Call<ApiResponse> getAdsByCompany(@Path("id_company") int companyId, @Header("Authorization") String token);

    @FormUrlEncoded
    @POST("v1/zonesbycompany")
    Call<ApiResponse> getZonesByComapany(@Field("id_company") int companyId, @Header("Authorization") String token);

    @POST("v1/savezone")
    Call<ApiResponse> saveZone(@Body Zone zone, @Header("Authorization") String token);

    @Multipart
    @POST("v1/savead")
    Call<ApiResponse> saveAd(@Part MultipartBody.Part videoFile, @Part MultipartBody.Part thumbnailFile, @Part("name")RequestBody name, @Part("description")RequestBody description, @Part("id_company")RequestBody idCompany,@Part("duration")RequestBody duration, @Header("Authorization") String token);

    @POST("v1/savecampaign")
    Call<ApiResponse> saveCampaign(@Body Campaign campaign, @Header("Authorization") String token);

    @POST("v1/updatecampaign/{id}")
    Call<ApiResponse> updateCampaign(@Path("id")int id,@Body Campaign campaign, @Header("Authorization") String token);

    @GET("v1/codes")
    Call<ApiResponse> getCodes(@Header("Authorization") String token);

    @POST("v1/savecode")
    Call<ApiResponse> saveCode(@Body Code code,@Header("Authorization") String token);

    @GET("v1/redeem/{idcompany}/{code}")
    Call<ApiResponse> redeemCode(@Path("idcompany") int idCompany, @Path("code") String code, @Header("Authorization") String token);

}
