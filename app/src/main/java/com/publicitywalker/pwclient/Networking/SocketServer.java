package com.publicitywalker.pwclient.Networking;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by juancho on 28/08/17.
 */

public class SocketServer {
    ServerSocket serverSocket;
    Context context;
    SocketServerListener listener;
    LatLng latLng;
    public static final int SocketServerPORT = 9090;

    public SocketServer(Context context, SocketServerListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void launchServer() {

        Thread socketServerThread = new Thread(new SocketServerThread());
        socketServerThread.start();
        LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        service.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 10000, 100, locationListener);
       Location location= service.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(location!=null)
            latLng= new LatLng(location.getLatitude(),location.getLongitude());

    }
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            latLng = new LatLng(location.getLatitude(),location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };



    private class SocketServerThread extends Thread {


        int count = 0;

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(SocketServerPORT);


                while (true) {
                    Socket socket = serverSocket.accept();
                    count++;

                   String message = null;
                    if(latLng!=null)
                    message =latLng.latitude+","+latLng.longitude;



                    BufferedReader input =
                            new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String answer = input.readLine();
                    System.out.println(answer);

                    PrintStream printStream = new PrintStream(socket.getOutputStream());
                    printStream.print(message);
                    listener.OnMessageReceived(answer);
                    printStream.close();
                    input.close();


                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }



    }
    public interface SocketServerListener {
        void OnMessageReceived(String message);
    }

}
