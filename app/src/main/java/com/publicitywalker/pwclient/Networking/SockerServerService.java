package com.publicitywalker.pwclient.Networking;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import com.publicitywalker.pwclient.Activities.HomeActivity;
import com.publicitywalker.pwclient.Fragments.WalkerFragment;
import com.publicitywalker.pwclient.Managers.AccessPointManager;
import com.publicitywalker.pwclient.R;

import java.lang.reflect.InvocationTargetException;

import static java.util.logging.Logger.global;

/**
 * Created by juancho on 1/09/17.
 */

public class SockerServerService extends Service {

    private SocketServer socketServer;
    private SocketServer.SocketServerListener homeSocketServerListener;
    private final IBinder mBinder = new LocalBinder();
    private static int NOTIFICATION_ID=1;
    private AccessPointManager accessPointManager;
    private Notification.Builder notificationBuilder;
    private NotificationManager notificationManager;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent normalIntent = new Intent(this, HomeActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                normalIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.logo)  // the status icon
                .setTicker(getString(R.string.server_active))  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                .setContentTitle(getText(R.string.server_active))
                .setContentText("Esperando Conexion")
                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
               ;
       notificationManager.notify(NOTIFICATION_ID,notificationBuilder.build());

        socketServer = new SocketServer(this,socketServerListener);
        accessPointManager = new AccessPointManager(this);
        try {
            accessPointManager.enableAccessPoint();
            socketServer.launchServer();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
       WalkerFragment.isServiceRunning = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        accessPointManager.disableAccessPoint();
        notificationManager.cancel(NOTIFICATION_ID);
        WalkerFragment.isServiceRunning = false;
    }

    private SocketServer.SocketServerListener socketServerListener = new SocketServer.SocketServerListener() {
      @Override
      public void OnMessageReceived(String message) {
          if(homeSocketServerListener!=null)
              homeSocketServerListener.OnMessageReceived(message);

          notificationBuilder.setContentText(message);
          notificationManager.notify(NOTIFICATION_ID,notificationBuilder.build());
      }
  };


    public class LocalBinder extends Binder {
        public SockerServerService getServiceInstance(){
            return SockerServerService.this;
        }
    }


    public void setHomeSocketServerListener(SocketServer.SocketServerListener listener){
      this.homeSocketServerListener = listener;
  }

}
