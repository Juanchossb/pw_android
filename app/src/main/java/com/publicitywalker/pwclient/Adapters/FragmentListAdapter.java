package com.publicitywalker.pwclient.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.publicitywalker.pwclient.Holders.FragmentListHolder;
import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.Model.Code;
import com.publicitywalker.pwclient.Model.Zone;
import com.publicitywalker.pwclient.R;
import com.publicitywalker.pwclient.UI.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by juancho on 26/07/17.
 */

public class FragmentListAdapter extends RecyclerView.Adapter<FragmentListHolder> {

    private List<Object> objectList;
    private static String SUBTITLE_CAMPAIG="Fecha de creación";
    private static String SUBITITLE_AD="Duración";
    private FragmentListAdapterListener listener;


    public FragmentListAdapter(List<Object> objects){
        this.objectList = objects;
    }
    @Override
    public FragmentListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_list_row, parent, false);
        return new FragmentListHolder(view);
    }

    @Override
    public void onBindViewHolder(FragmentListHolder holder, int position) {
        Object object = objectList.get(position);
        if(object.getClass() == Campaign.class){
            Campaign campaign = (Campaign) object;
            holder.title.setText(campaign.getName());
            holder.description.setText(campaign.getDescription());
            holder.subtitle.setText(SUBTITLE_CAMPAIG);
            holder.subtext.setText(campaign.getCreationDate());
            if(campaign.getThumbnail().length()>0)
            Picasso.with(holder.image.getContext()).load(campaign.getThumbnail()).fit().into(holder.image);
        }else if(object.getClass() == Ad.class){
            final Ad ad = (Ad) object;
            holder.title.setText(ad.getName());
            holder.description.setText(ad.getDescription());
            holder.subtitle.setText(SUBITITLE_AD);
            holder.subtext.setText(ad.getDurationsInMins());
            Picasso.with(holder.image.getContext()).load(R.drawable.video).fit().into(holder.image);
            if(ad.getThumbnail().length()>0)
                Picasso.with(holder.image.getContext()).load(ad.getThumbnail()).transform(new CircleTransform()).fit().into(holder.image);
            else
                Picasso.with(holder.image.getContext()).load(R.drawable.video).transform(new CircleTransform()).fit().into(holder.image);

            holder.line.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null)
                        listener.onAdCicked(ad);
                }
            });
        }else if (object.getClass() == Zone.class){
            Zone zone = (Zone) object;
            holder.title.setText(zone.getName());
            holder.description.setText(zone.getDescription());
           // holder.subtitle.setText(SUBTITLE_ZONE);
            holder.subtext.setText(zone.getFormattedArea()+" mts2");
            Picasso.with(holder.image.getContext()).load(R.drawable.map).fit().into(holder.image);
        }else if (object.getClass() == Code.class){
            Code code = (Code) object;
            String title = String.format("%s [%s][%d]",code.getCompanyName(),code.getCode(),code.getAmount());
            holder.title.setText(title);
            String description = String.format("%s - tel: %s",code.getUserName(),code.getPhone());
            holder.description.setText(description);
            holder.subtext.setText(code.getNotes());
            Picasso.with(holder.image.getContext()).load(R.drawable.code).fit().into(holder.image);

            if(code.isActive()){
                holder.subtitle.setText("activo");
                holder.subtitle.setTextColor(holder.subtitle.getContext().getColor(R.color.colorAccept));
            }else{
                holder.subtitle.setText("inactivo");
                holder.subtitle.setTextColor(holder.subtitle.getContext().getColor(R.color.colorCancel));

            }

        }

    }

    public void setListener(FragmentListAdapterListener listener){
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    public interface FragmentListAdapterListener{
        void onAdCicked(Ad ad);
    }
}
