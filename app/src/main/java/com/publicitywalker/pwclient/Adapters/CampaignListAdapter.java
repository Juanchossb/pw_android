package com.publicitywalker.pwclient.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.publicitywalker.pwclient.Holders.CampaignListHolder;
import com.publicitywalker.pwclient.Model.Campaign;
import com.publicitywalker.pwclient.R;
import com.publicitywalker.pwclient.UI.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by juancho on 10/08/17.
 */

public class CampaignListAdapter extends RecyclerView.Adapter<CampaignListHolder> {
    List<Campaign> campaignList;
    CampaigListAdapterlistener listAdapterlistener;

    private static String SUBTITLE_CAMPAIGN="Activado: ";

    public CampaignListAdapter(List<Campaign> campaignList){
        this.campaignList = campaignList;
    }
    @Override
    public CampaignListHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new CampaignListHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.campaign_list_row,parent,false));
    }

    @Override
    public void onBindViewHolder(CampaignListHolder holder, int position) {

        Campaign campaign = campaignList.get(position);
        holder.title.setText(campaign.getName());
        holder.description.setText(campaign.getDescription());
        holder.subtitle.setText(SUBTITLE_CAMPAIGN);
        if(campaign.getThumbnail().length()>0)
            Picasso.with(holder.image.getContext()).load(campaign.getThumbnail()).transform(new CircleTransform()).fit().into(holder.image);

        holder.rowSwitch.setChecked(campaign.isActive());
        holder.campaign = campaign;
        holder.rowSwitch.setTag(holder);
        holder.rowSwitch.setOnCheckedChangeListener(checkedChangeListener);

        holder.played.setText(campaign.getNumberOfAdsPlayed()+"");
        holder.amount.setText(String.format("%.2f/%.2f",campaign.getPointsConsumed(),campaign.getInvestment()));
        holder.porcentage.setText(String.format("%.1f",campaign.getPortcetageConsumed())+"%");

        holder.progressBar.setMax(100);
        holder.progressBar.setProgress((int) campaign.getPortcetageConsumed());

    }

    @Override
    public int getItemCount() {
        return campaignList.size();
    }

    private CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            CampaignListHolder holder = (CampaignListHolder) buttonView.getTag();
            Campaign campaign = holder.campaign;
            campaign.setActive(isChecked);
            if(listAdapterlistener!=null)
            listAdapterlistener.OnCheckedSwitchChanged(campaign);

        }
    };

    public interface CampaigListAdapterlistener{
        void OnCheckedSwitchChanged(Campaign campaign);
    }

    public void setListener(CampaigListAdapterlistener listener){
        this.listAdapterlistener = listener;
    }

    public void updateList(List<Campaign> campaignList){
        this.campaignList.clear();
        this.campaignList.addAll(campaignList);
        this.notifyDataSetChanged();
    }


}
