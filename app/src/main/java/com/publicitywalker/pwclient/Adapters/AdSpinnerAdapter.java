package com.publicitywalker.pwclient.Adapters;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.publicitywalker.pwclient.Model.Ad;
import com.publicitywalker.pwclient.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by juancho on 1/08/17.
 */

public class AdSpinnerAdapter extends BaseAdapter {

    private List<Ad> adList;
    private Context context;


    public AdSpinnerAdapter (Context context , List<Ad> adList){
        this.context = context;
        this.adList = adList;
    }


    @Override
    public int getCount() {
        return adList.size();
    }

    @Override
    public Object getItem(int position) {
        return adList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return adList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Ad ad = adList.get(position);
        if(convertView==null)
            convertView = LayoutInflater.from(context).inflate(R.layout.ad_spinner_row,null);

        ImageView image = (ImageView) convertView.findViewById(R.id.ad_spinner_image);
        TextView text = (TextView) convertView.findViewById(R.id.ad_spinner_text);

        if(ad.getThumbnail().length()>0)
            Picasso.with(context).load(ad.getThumbnail()).fit().into(image);

        text.setText(ad.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        Ad ad = adList.get(position);
        if(convertView==null)
            convertView = LayoutInflater.from(context).inflate(R.layout.ad_spinner_row,null);

        ImageView image = (ImageView) convertView.findViewById(R.id.ad_spinner_image);
        TextView text = (TextView) convertView.findViewById(R.id.ad_spinner_text);

        if(ad.getThumbnail().length()>0)
            Picasso.with(context).load(ad.getThumbnail()).fit().into(image);

        text.setText(ad.getName());

        return convertView;
    }
}
