package com.publicitywalker.pwclient.Adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polygon;
import com.publicitywalker.pwclient.Holders.ZoneListHolder;
import com.publicitywalker.pwclient.Model.Zone;
import com.publicitywalker.pwclient.R;

import java.util.List;

/**
 * Created by juancho on 28/07/17.
 */

public class ZoneListAdapter extends RecyclerView.Adapter<ZoneListHolder> implements View.OnClickListener{
    private List<Zone> zoneList;
    private static String SUBTITLE_ZONE="Area";
    private ZoneListClickLstener clickLstener;
    public ZoneListAdapter(List<Zone> zones){
        this.zoneList = zones;
    }

    @Override
    public ZoneListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ZoneListHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.zone_list_row,parent,false));
    }

    @Override
    public void onBindViewHolder(final ZoneListHolder holder, int position) {
        final Zone zone = zoneList.get(position);
        holder.zone = zone;
        holder.title.setText(zone.getName());
        holder.description.setText(zone.getDescription());
        holder.subtitle.setText(SUBTITLE_ZONE);
        holder.subtext.setText(zone.getFormattedArea()+" mts2");
        holder.mapView.onCreate(null);
        holder.parent.setTag(holder);
        holder.parent.setOnClickListener(this);


        if(holder.mapView!=null)
            holder.mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    Polygon polygon = googleMap.addPolygon(zone.getPolygon(holder.mapView.getContext()));
                    int padding = 0; // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(zone.getMapBounds(), padding);
                    try {
                        googleMap.moveCamera(cu);
                        holder.mapView.onResume();
                        holder.mapView.onEnterAmbient(null);
                    }catch (IllegalStateException e){

                    }
                }
            });


    }

    public void setZoneListClickListener(ZoneListClickLstener listClickListener){
        this.clickLstener = listClickListener;
    }

    @Override
    public int getItemCount() {
        return zoneList.size();
    }

    @Override
    public void onClick(View v) {

        ZoneListHolder holder = (ZoneListHolder) v.getTag();
       if(clickLstener!=null) {
           //clickLstener.OnZoneClicked(holder.zone);
       }
    }

    public interface ZoneListClickLstener{
        void OnZoneClicked(Zone zone);
    }
}
