package com.publicitywalker.pwclient.Managers;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by juancho on 28/08/17.
 */

public class AccessPointManager {

    private WifiConfiguration netConfig;
    private WifiManager wifiManager;

    public AccessPointManager(Context context) {
        netConfig = new WifiConfiguration();
        netConfig.SSID = "PublicityWalker";
        netConfig.preSharedKey = "pw123456.,";
        //
        // netConfig.hiddenSSID = true;
        netConfig.status = WifiConfiguration.Status.ENABLED;
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

    }

    public int enableAccessPoint() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method setWifiApMethod = wifiManager.getClass().getMethod("setWifiApEnabled",  WifiConfiguration.class, boolean.class);
        boolean apstatus=(Boolean) setWifiApMethod.invoke(wifiManager, netConfig,true);

        Method isWifiApEnabledmethod = wifiManager.getClass().getMethod("isWifiApEnabled");
        while(!(Boolean)isWifiApEnabledmethod.invoke(wifiManager)){};
        Method getWifiApStateMethod = wifiManager.getClass().getMethod("getWifiApState");
        int apstate=(Integer)getWifiApStateMethod.invoke(wifiManager);
        Method getWifiApConfigurationMethod = wifiManager.getClass().getMethod("getWifiApConfiguration");
        netConfig=(WifiConfiguration)getWifiApConfigurationMethod.invoke(wifiManager);
        Log.e("CLIENT", "\nSSID:"+netConfig.SSID+"\nPassword:"+netConfig.preSharedKey+"\n");
        return apstate;
    }

    public void disableAccessPoint(){
        try {
            Method setWifiApMethod = wifiManager.getClass().getMethod("setWifiApEnabled",  WifiConfiguration.class, boolean.class);
            boolean apstatus=(Boolean) setWifiApMethod.invoke(wifiManager, netConfig,false);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
